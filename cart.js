(function() {

    var MyCartApp = angular.module("MyCartApp", []);

    //As a function constructor
    var GiphyService = function($http, $q) {
        var giphyService = this;
        giphyService.search = function(tag, apiKey) {
            /*
                var defer = $q.defer();
                var p = new Promise(function(resolve, reject) {

                }) 
            */
            var defer = $q.defer(); //Creating a promise
            $http.get("http://api.giphy.com/v1/gifs/search", {
                params: {
                    api_key: "db011799f8e4427c9a723399529bfb6a",
                    q: tag
                }
            }).then(function(result) {
                console.log(">>> retured from api.giphy")
                var data = result.data.data;
                var images = []
                for (var i in data)
                    images.push(data[i].images.downsized_medium.url);
                console.log(">>> resolving the promise: passing images back")
                console.log("\t ", images)
                defer.resolve(images);

            }).catch(function(err) {
                defer.reject(err);
            });
            return (defer.promise);
        };
    }

    var MyCartCtrl = function(GiphyService) {
        var myCartCtrl = this;

        myCartCtrl.item = "";
        myCartCtrl.items = [];

        myCartCtrl.addToCart = function() {
            console.log("clicked!: %s", myCartCtrl.item);
            GiphyService.search(
                myCartCtrl.item, "__your_API_key_here__"

            ).then(function(result) {
                console.log("Got result from GiphyService.search")
                myCartCtrl.items = result;

            }).catch(function(err) {
                console.error("err = ", err)
            });

            console.log("after calling search()");
        }

    }

    MyCartApp.service("GiphyService", ["$http", "$q", GiphyService])
    MyCartApp.controller("MyCartCtrl", ["GiphyService", MyCartCtrl]);

})();